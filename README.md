# Applications WEB, MVC, Services Rest
 ++ Pré-requis au Développement d'une application Web
 

## Contexte
* Au programme de ce cours: Génération et Parsage JSON 

Notre startup a fait le buzz auprès de ses investisseurs grâce au prototype développé avec Swing. 
Le hic, c'est qu'à présent, pour avancer, nous devons accepter l'introduction d'un nouvel investisseur prenant le rôle de CTO, car ayant la confiance du BOard en matière d'industrialisation logicielle.
Ce nouveau CTO ainsi proclamé, un certain nombre de pré-requis nous sont alors imposés.

 1. prérequis n°1: passer par une architecture centralisée Web 2.0. Incontournablement, afin de pouvoir progresser sur un business model rentable, évoluer ver le Web semble la meilleure option. On touche ainsi une plus large clientèle, avec des possibilités de monétisation bien plus efficaces.
 2. prérequis n°2: utiliser le format RFC8259 (JSON) de façon stricte et généralisée pour toute sérialisation ou stockage d'information. Peut-être notre CTO aurait-il des ambitions à plus long terme avec un passage sur le cloud potentiellement plus efficace ?
 3. ...

## Objectifs
* Mises en application:
 - [x] **(Exercice 1) Normalisation JSON en pré-requis au passage en application WEB**
 - [ ] (Exercice 2) BONUS de méthodes utilitaires, exceptions pour détection de fraude, Utilisation d'un Tableau associatif
 - [ ] (Exercice 3) Homogénéisation Maven + Spring 
 - [ ] (Exercice 4) Services Rest et Scaffolding web basique
----

## Normalisation JSON

Pour commencer cette première étape de la transition vers une application Web, observons le Diagramme Classe du design que propose notre CTO:

```plantuml
@startuml

title __JSON Serialization's Class Diagram__\n


  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    namespace impl {
      namespace json {
        class fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl {
            {static} - LOG : Logger
            - walletDatabase : File
            + JSONBadgeWalletDAOImpl()
            + addBadge()
            + addBadge()
            + getBadge()
            + getBadgeFromMetadata()
            + getWalletMetadata()
            - getWalletMetadata()
        }
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.wallet.dao {
    interface fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO {
        {abstract} + addBadge()
        {abstract} + getBadgeFromMetadata()
        {abstract} + getWalletMetadata()
    }
  }

  fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl .up.|> fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO


  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.serializer{
      namespace impl.db {
            namespace json {
              class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl {
                  ~ metas : Set<DigitalBadge>
                  {static} - LOG : Logger
                  - sourceOutputStream : OutputStream
                  + JSONWalletDeserializerDAImpl()
                  + deserialize()
                  + deserialize()
                  + getDeserializingStream()
                  + getSourceOutputStream()
                  + setSourceOutputStream()
              }
            }
      }
    }
  }
  

  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.serializer {
      namespace impl.db {
        namespace json {
          class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl {
              ~ metas : Set<DigitalBadge>
              + JSONWalletSerializerDAImpl()
              + getSerializingStream()
              + getSourceInputStream()
              + serialize()
          }
        }
      }
    }
  }

  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.serializer {
      namespace impl.db {
          namespace json {
              class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl {
                  {static} - LOG : Logger
                  + deserialize()
              }
          }
      }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.fileutils {
      namespace streaming.serializer {
        abstract class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer {
            + serialize()
        }
    }
  }

  namespace fr.cnam.foad.nfa035.badges.fileutils {
      namespace streaming.serializer {
          interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer {
            {abstract} + deserialize()
            {abstract} + getSourceOutputStream()
            {abstract} + setSourceOutputStream()
          }
      }
  }



  namespace fr.cnam.foad.nfa035.badges.fileutils {
      namespace streaming.serializer {
          interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer {
            {abstract} + getSerializingStream()
            {abstract} + getSourceInputStream()
            {abstract} + serialize()
          }
      }
  }


  namespace fr.cnam.foad.nfa035.badges.fileutils {
      namespace streaming.serializer {
        interface fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer {
            {abstract} + deserialize()
        }
      }
  }



  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.serializer.impl.db {
        class fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl {
            {static} - LOG : Logger
            + deserialize()
            {static} + readLastLine()
        }
      }
  }

  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl -up-|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer
  fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl --|> fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl


  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.media {
        namespace impl {
          namespace json {
            class fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame {
                {static} - LOG : Logger
                - encodedImageOutput : FileOutputStream
                + JSONWalletFrame()
                + getEncodedImageOutput()
            }
          }
        }
    }
  }


  fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame -up-|> fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame

  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.media {
        interface fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia {
            {abstract} + getNumberOfLines()
            {abstract} + incrementLines()
        }
    }
  }


  namespace fr.cnam.foad.nfa035.badges.fileutils {
    namespace streaming.media {
        namespace impl {
          class fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame {
              {static} - LOG : Logger
              - encodedImageOutput : FileOutputStream
              - encodedImageReader : BufferedReader
              - numberOfLines : long
              + WalletFrame()
              + getEncodedImageInput()
              + getEncodedImageOutput()
              + getEncodedImageReader()
              + getNumberOfLines()
              + incrementLines()
              # setNumberOfLines()
          }
        }
    }
  }

  fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame .up.|> fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia

  fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl .up.> fr.cnam.foad.nfa035.badges.fileutils

@enduml

```

Réfléchissons à présent sur notre sujet, si de CSV on passe à JSON, on peut relever le défi de portage de notre code existant en posant les choses ainsi:
 * Chaque enregistrement tient sur une ligne, comme toujours, le json ne sera donc pas indenté.
 * Il peut donc s'agir d'un tableau d'objet voir d'un tableau de tableaux d'objets, car nous voulons, à des fins d'optimisation évidentes, pouvoir parser les métadonnées indépendament dela payload (c'est-à-dire les images encodées).
 [ ] Appliquons donc la structure suivante par exmple:
   ```json
   [
      [
         {
            "badge":{
               "metadata":{
                  "badgeId":4,
                  "walletPosition":49962,
                  "imageSize":-1
               }
            }
         },
         {
            "payload":null
         }
      ]
   ]
   ```
 - [ ] A tester sur https://jsonformatter.curiousconcept.com/# par exemple
 - [ ] A plat cela donerait:
   ```json
   [[{"badge":{"metadata":{"badgeId":4,"walletPosition":49962,"imageSize":-1}}},{"payload":null}]]
   ```
 - [ ] Mais si l'on avait n lignes, par exemple 3, alors:
   ```json
   [[{"badge":{"metadata":{"badgeId":1,"walletPosition":0,"imageSize":557},"serial":"NFA033","begin":1632693600000,"end":1609628400000}},{"payload":"Image encodé e en Base 64..."}],
   [{"badge":{"metadata":{"badgeId":2,"walletPosition":895,"imageSize":906},"serial":"NFA034","begin":1632693600000,"end":1609628400000}},{"payload":"Image encodé e en Base 64.............."}],
   [{"badge":{"metadata":{"badgeId":3,"walletPosition":2255,"imageSize":35664},"serial":"NFA035","begin":1632693600000,"end":1609628400000}},{"payload":"Image encodé e en Base 64..."}],
   [{"badge":{"metadata":{"badgeId":4,"walletPosition":49962,"imageSize":-1}}},{"payload":null}]]
   ```
 - [ ] Ainsi on garantit un fichier de base json conforme à RFC-8259
 - [ ] Cela donnera par exmple un **MetadataDeserializerJSONImpl** avec le méthode suivante:
  ```java
      public class MetadataDeserializerJSONImpl extends MetadataDeserializerDatabaseImpl implements MetadataDeserializer {
         private static final Logger LOG = LogManager.getLogger(MetadataDeserializerJSONImpl.class);
         @Override
         public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            BufferedReader br = media.getEncodedImageReader(false);
            JsonFactory jsonFactory = new JsonFactory();
            jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
            ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
            return br.lines()
                    .map(
                            l-> {
                               try {
                                  String badge = l.split(",\\{\"payload")[0].split(".*badge\":")[1];
                                  DigitalBadge digitalBadge = objectMapper.readValue(badge, DigitalBadge.class);
                                  return digitalBadge.getMetadata().getImageSize() == -1 ? null : digitalBadge;
                               } catch (IOException ioException) {
                                  LOG.error("Problème de parsage JSON, on considère l'enregistrement Nul", ioException);
                               }
                               return null;
                            }
                    ).filter(x -> x!=null).collect(Collectors.toSet());
         }
   }
  ```
 - [ ] Et si la désérialisation du **JSONWalletDeserializerDAImpl** codée par vote collègue qui est partit en vacances hier est ainsi faite, saurez-vous sérialiser ?
  ```java
   @Override
   public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {
   
        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }
        targetBadge.setSerial(badge.getSerial());
        targetBadge.setBegin(badge.getBegin());
        targetBadge.setEnd(badge.getEnd());
    }

  ```
 - [ ] Bien sûr la preuve est dans le test (mais aussi dans le code que vous produisez ;) ), alors à vos claviers et merci encore pour vos chefs d'oeuvres personnels :) 
   ![preuve][preuve]
 - [ ] *J'ai commené par importer les dépendances JSON pour Maven :*
 - ![maven2][]
 - [ ] *Les tests fournis pour l'exercice 4-1 n'incuent pas les sections JSON (11 tests au lieu de 17) :*
 - ![test1][]
 - *Note: Tous les packages Json de l'arboreecence se sont mis à ne plus reconnaître les "imports" des autres packages pour je ne sais quelle raison =( :*
- ![probleme][]

[preuve]: screenshots/All-Tests-3.png "17 tests au vert"
[maven2]: screenshots/maven2.png "importation JSON"
[test1]: screenshots/test1.png "11 anciens tests"
[probleme]: screenshots/probleme.png "problème importation package"

----
