package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;




public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private File walletDatabase;

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    public JSONBadgeWalletDAOImpl(File walletDatabase) {
        this.walletDatabase = walletDatabase;
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException {

    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {

    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {

    }

    /**
     * Permet de récupérer les metadonnées du Wallet
     *
     * @return List<DigitalBadgeMetadata>
     * @throws IOException
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        return null;
    }

    /**
     * Permet de récupérer un badge du Wallet à partir de ses métadonnées
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {

    }
}
