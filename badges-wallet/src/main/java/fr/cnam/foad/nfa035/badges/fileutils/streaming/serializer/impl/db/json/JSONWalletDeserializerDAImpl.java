package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

/**
 * Classe de désérialisation de JSON
 */
public class JSONWalletDeserializerDAImpl implements DirectAccessDatabaseDeserializer {

    private Set<DigitalBadge> metas;

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    private OutputStream sourceOutputStream;


    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }
        targetBadge.setSerial(badge.getSerial());
        targetBadge.setBegin(badge.getBegin());
        targetBadge.setEnd(badge.getEnd());
    }

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @return
     */
    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    /**
     * Utile pour modifier le flux de sortie au format source
     *
     * @param os
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }
}
