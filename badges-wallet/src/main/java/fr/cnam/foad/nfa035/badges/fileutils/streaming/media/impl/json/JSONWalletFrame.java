package fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

public class JSONWalletFrame extends WalletFrame {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrame.class);

    private FileOutputStream encodedImageOutput = null;

    /**
     * Constructeur élémentaire
     *
     * @param walletDatabase
     */
    public JSONWalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }



}

