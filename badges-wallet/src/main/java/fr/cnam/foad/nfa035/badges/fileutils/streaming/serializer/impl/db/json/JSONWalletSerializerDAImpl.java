package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class JSONWalletSerializerDAImpl extends AbstractStreamingImageSerializer {

    private Set<DigitalBadge> metas;

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {
        return null;
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return null;
    }
}
